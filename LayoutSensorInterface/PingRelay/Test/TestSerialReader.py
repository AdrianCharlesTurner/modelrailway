import serial
from datetime import datetime
import sys

if len(sys.argv) > 1:
   sport = sys.argv[1]
else:
   sport = 'COM4'

ser = serial.Serial(
            port='\\.\%s' % sport,
            baudrate=9600,
            parity=serial.PARITY_ODD,
            stopbits=serial.STOPBITS_TWO,
            bytesize=serial.SEVENBITS,
            timeout=10)
print "Listening to " + sport + "...\n"
while True:
   data = ser.read()
   if data != '':
      print "---READ---"
      # print "raw : ", data
      print "ord : ", ord(data)
      print "time: ", datetime.now()
      print "----------"
      print
