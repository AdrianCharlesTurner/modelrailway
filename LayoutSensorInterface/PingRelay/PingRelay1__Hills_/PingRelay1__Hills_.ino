/*
There are 56 I/O pins per Arduino Mega - we must assign each Arduino a set of reed switches to monitor.
Separate sketches?

Need a static map to relate the pin number to the lat:long of the switch.
This could be in the Java - it depends on what we want to report to the TBU.

Up Main either {30, 36, 35, 24} or reverse.

*/
// In case there are non-continuous sets of pins.
int static const pinCount = 18;
int pins[pinCount] = {31, 29, 27, 25, 23, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 33, 35, 37};
int pinToReed[pinCount] = {4, 3, 2, 1, 0, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 20, 45, 17};
int reads[pinCount] = { 0 };
int counts[pinCount] = { 0 };
int prevReads[pinCount] = { 0 };  // array that holds previous readouts
int i;                // variable used in for-loops

// For Debounce we have a time delay. unsigned longs becasue the time, measured in 
// milliseconds will overflow int.
unsigned long lastDebounceTime = 0;
unsigned long lastDebounceTimes[pinCount] = { 0 };
// Increase this if picking up mulitple reads from one sensor and decrease if skipping
// the read from other side of the train - this will be a balance and hopefully the
// speed of the train is not great enough to trigger reed from front and rear in less
// time than is needed to debounce.
unsigned long debounceDelay = 400;

void setup() {
  Serial.begin(9600);
  for (i = 0; i < pinCount; i++) {
    pinMode(pins[i], INPUT_PULLUP);  // Make sure this is INPUT_PULLUP
    reads[i] = HIGH;
    prevReads[i] = HIGH;
  }
}

void loop() {
  monitorDebounced();
//  monitorDebouncedFilter();
}

void monitorDebounced() {
  for (i = 0; i < pinCount; i++) {
    // Read the pin
    reads[i] = digitalRead(pins[i]);
    // If the pin has changed state, inspect timing
    if (reads[i] != prevReads[i]) {
      // Reset the debouncing timer
      if (reads[i] == LOW && (millis() - lastDebounceTimes[i]) > debounceDelay) {
        Serial.write(int(pinToReed[i]));
        lastDebounceTimes[i] = millis();
      }
      prevReads[i] = reads[i];
    }
  }
}

void monitorDebouncedFilter() {
  for (i=0; i < pinCount; i++) {
    // Read the pin
    reads[i] = digitalRead(pins[i]);
    // If the pin has changed state, inspect timing
    if (reads[i] != prevReads[i]) {
      counts[i]++;
    }
  }
}


