#!/usr/bin/python2
#------------------------------------------------------------------------------
"""
   This is the main for the progam that will read pings from arduinos and
   cross-reference the sensor number with static GPS locations, and then package
   the location into a message that the TBUs will understand and send to the 
   correct TBU.

   There is scope for this program to communicate with the JMRI software so that
   the train location can be updated in the layout control software.

   This main will simply initialise the sniffers, senders and a processor that
   will package the messages.
   The sniffer -> processor -> sender threads will need to communicate.
   They will use queues to allow us to extend how many channels to sniff and
   TBUs to process for.
"""
#------------------------------------------------------------------------------
import time
import sys
import threading
import Tkinter as tk
from datetime import datetime
from Queue import Queue

from Config import CommsParams
from Config import MessageTemplates
from Classes.SerialReader import SerialReader
from Classes.Sender import Sender
from Classes.MessagePacker import MessagePacker

#------------------------------------------------------------------------------


class LocationPingPackager():
   def __init__(self):
      # Not much to initialise
      print "|------------------------------------------------------------------------------"
      print "|           .---- -  -"
      print "|          (   ,----- - -"
      print "|           \_/      ___"
      print "|         c--U---^--'o  [_           Model Train Ping Packager"
      print "|         |------------'_|   "
      print "|        /_(o)(o)--(o)(o)'"
      print "|  ~ ~~~~~~~~~~~~~~~~~~~~~~~~ ~"
      print "|------------------------------------------------------------------------------"
      # THE lock
      self.lock = threading.Lock()
      # Print out lock
      self.print_lock = threading.Lock()
      self.threads = []

   def start_receiver_threads(self):
      """ Split the thread starting into functional sections as to add 
          structure and some kind of readability for maintenance.
      """
      if not CommsParams.TEST_MODE:
         for i in range(0, CommsParams.NUM_SERVERS):
            # Relying on lined up indices :s
            recv_thread = SerialReader(self.print_lock,
                                       CommsParams.QUEUES_IN[i],
                                       CommsParams.SPORTS[i])
            recv_thread.start()
            self.threads.append(recv_thread)

   def start_sending_threads(self):
      """ Split the thread starting into functional sections as to add 
          structure and some kind of readability for maintenance.
      """
      for i in range(0, CommsParams.NUM_CLIENTS):
         # Relying on lined up indices :s
         sender_thread = Sender(
            self.print_lock, "TCP/IP", self.lock, CommsParams.QUEUES_OUT[i],
            CommsParams.IP_CLIENTS[i], CommsParams.PORT_CLIENT)
         sender_thread.start()
         self.threads.append(sender_thread)

   def start_worker_threads(self):
      """ Split the thread starting into functional sections as to add 
          structure and some kind of readability for maintenance.
      """
      thrd = MessagePacker(self.print_lock, self.lock,
                           CommsParams.QUEUES_IN[0], CommsParams.QUEUES_OUT)
      thrd.start()
      self.threads.append(thrd)

   def pause_sim(self, paused):
      """
      """
      for thrd in self.threads:
         if str(thrd) == "MessagePacker":
            if paused:
               thrd.pause()
            else:
               thrd.resume()

   def send_reset(self):
      """ Bang a reset on the out_queue.
      """
      for i in range(0, CommsParams.NUM_CLIENTS):
         CommsParams.QUEUES_OUT[i].put(MessageTemplates.make_Reset())
      for thrd in self.threads:
         if str(thrd) == "MessagePacker":
            if thrd.is_paused():
               thrd.stop()
               thrd.resume()

   def clear_queues(self):
      """ Clears all the items already on the queues.
      """
      for q in CommsParams.QUEUES_OUT:
         with q.mutex:
            q.queue.clear()
      for q in CommsParams.QUEUES_IN:
         with q.mutex:
            q.queue.clear()

#------------------------------------------------------------------------------


class GUI():
   """ 
   """
   def __init__(self, master):
      self.master = master
      self.master.bind_all("<Control-r>", self.reset_short)
      self.main_program = None
      self.title = "Location Messenger"
      master.title(self.title)
      master.resizable(False, True)
      master.minsize(width=650, height=200)
      master.maxsize(width=1050, height=2000)
      master.protocol("WM_DELETE_WINDOW", self.disable_event)
      # Fonts
      font1 = ('Consolas', 18, 'bold')
      # Buttons
      self.buttons = {}
      # Reset Button
      self.buttons["reset"] = tk.Button(
         master,
         text="reset",
         command=self.reset,
         relief=tk.SOLID,
         borderwidth=1,
         background="lightskyblue",
         foreground="gray15",
         font=font1,
         width=10)
      # Exit Button
      self.buttons["exit"] = tk.Button(
         master,
         text="exit",
         command=self.exit,
         relief=tk.SOLID,
         borderwidth=1,
         background="darkorange",
         foreground="gray15",
         font=font1,
         width=10)
      # If debug mode add the string to the title
      if CommsParams.DEBUG_MODE:
         self.title += " [DEBUG_MODE]"
      # If test mode add a pause button for the simulated data
      self.paused = False
      if CommsParams.TEST_MODE:
         self.title += " [TEST MODE]"
         self.buttons["pause"] = tk.Button(
            master,
            text="pause",
            command=self.pause,
            relief=tk.SOLID,
            borderwidth=1,
            background="black",
            foreground="white",
            font=font1,
            width=10)
      self.place_buttons(self.buttons)
      # STDOUT Capture console
      yscrollbar = tk.Scrollbar(master)
      yscrollbar.grid(column=2, row=0, rowspan=len(self.buttons), sticky='NSE')
      self.console = ThreadSafeText(
         master,
         wrap='word',
         background="gray15",
         foreground="white",
         yscrollcommand=yscrollbar.set,
         width=100)
      self.console.grid(column=1, row=0, rowspan=len(self.buttons), sticky='NSEW')
      yscrollbar.config(command=self.console.yview)
      master.grid_rowconfigure(0, weight=1)
      master.grid_rowconfigure(1, weight=1)
      master.grid_rowconfigure(2, weight=1)
      master.grid_columnconfigure(0, weight=1)
      master.grid_columnconfigure(1, weight=2)
      master.grid_columnconfigure(1, weight=1)
      # Set title after mode strings have been added.
      master.title(self.title)
      # Redirect stdout to this console.
      sys.stdout = StdoutRedirector(self.console)
      # Done with GUI - start main
      print "| GUI initialised"
      self.start_main()

   def start_main(self):
      """
      """
      # Init the one and only instance of main
      self.main_program = LocationPingPackager()
      print "| Starting worker threads..."
      self.main_program.start_receiver_threads()
      self.main_program.start_sending_threads()
      self.main_program.start_worker_threads()
      print "| Main program started @ %s" % datetime.now()

   def reset_short(self, event):
      """
      """
      self.reset()

   def reset(self):
      """ Reset the prgram and reset the threads.
      """
      self.main_program.send_reset()
      # Wait a second to get the message through before killing the threads.
      #time.sleep(2)
      if self.paused:
         self.buttons["pause"].config(text="pause")
         self.paused = False
      print "| GUI resetting @ %s" % datetime.now()
      for t in self.main_program.threads:
         print "STOP: ", t
         t.stop()
      for t in self.main_program.threads:
         print "JOIN: ", t
         t.join()
      # Clear the queues
      self.main_program.clear_queues()
      # Restart
      self.start_main()

   def exit(self):
      """ Kill the threads and close sockets and ports before exit.
      """
      print "| GUI exit"
      for t in self.main_program.threads:
         t.stop()
      for t in self.main_program.threads:
         t.join()
      # Exit
      sys.exit()

   def pause(self):
      """
      """
      if self.paused:
         self.buttons["pause"].config(text="pause")
         self.paused = False
      else:
         self.buttons["pause"].config(text="resume")
         self.paused = True

      self.main_program.pause_sim(self.paused)

   def disable_event(self):
      """ Do nothing.
      """
      print "| Close using the GUI \"EXIT\" button!"
      pass

   def place_buttons(self, buttons):
      """ Generic button placer.
      """
      i = 0
      for button in buttons:
         buttons[button].grid(column=0, row=i, sticky='NSEW')
         i += 1



class ThreadSafeText(tk.Text):
   def __init__(self, master, **options):
      tk.Text.__init__(self, master, **options)
      self.queue = Queue()
      self.update_me()

   def write(self, line):
      self.queue.put(line)

   def update_me(self):
      while not self.queue.empty():
         line = self.queue.get_nowait()
         self.insert(tk.END, line)
         self.see(tk.END)
         self.update_idletasks()
      self.after(100, self.update_me)


class StdoutRedirector():
   def __init__(self, widget):
      self.widget = widget

   def write(self, string):
      self.widget.write(string)


#------------------------------------------------------------------------------


def main():
   """ The main program simply starts threads and waits for them to die.
   """

   # for the purposes of running tests, we have an optional argument to set the
   # duration we wish for the BDCS to live.
   if len(sys.argv) > 1:
      life_span = float(sys.argv[1])
   else:
      life_span = -1.0

   # Init the GUI
   root = tk.Tk()
   gui = GUI(root)

   # We can stop the threads for test convenience
   if life_span >= 0.0:
      time.sleep(life_span)
      for t in gui.main_program.threads:
         t.stop()

   # Loop
   root.mainloop()

   # Dead
   print "| RIP in pepperonis threads, nvr 4get."
   sys.exit()


if __name__ == "__main__":
   main()
