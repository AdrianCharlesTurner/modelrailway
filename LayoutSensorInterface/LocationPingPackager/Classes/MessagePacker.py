#!/usr/bin/python2
#------------------------------------------------------------------------------
""" Takes from one in queue but can put on any out queues.

"""
#------------------------------------------------------------------------------
import threading
import Queue
from datetime import datetime
import time
import math

from SensorStateTable import SensorStateTable
from Config import MessageTemplates
from Config import SensorMap
from Config import CommsParams
if CommsParams.TEST_MODE:
   from Test.TestSerialInputs import TestSerialInputs

#------------------------------------------------------------------------------


class MessagePacker(threading.Thread):
   def __init__(self, print_lock, lock, in_queue, out_queues):
      threading.Thread.__init__(self)
      self.print_lock = print_lock
      self.lock = lock
      self.sensor_map = SensorMap().get_instance()
      self.in_queue = in_queue
      self.out_queues = out_queues
      self.running = True
      self.paused = False
      self.pause_cond = threading.Condition(threading.Lock())
      self.STATE = SensorStateTable(print_lock)
      if CommsParams.TEST_MODE:
         TestSerialInputs(self.in_queue)
      _send_initial_locations(self.STATE, self.out_queues, self.sensor_map)

   def __str__(self):
      return "MessagePacker"

   def run(self):
      while self.running:
         time.sleep(0.1)
         with self.pause_cond:
            while self.paused:
               self.pause_cond.wait()
            if CommsParams.TEST_MODE:
               time.sleep(2)
            key_str = ''
            # Get from the input queue
            with self.lock:
               try:
                  key_str = self.in_queue.get(False)
               except Queue.Empty:
                  pass
            # Check is int and in range of sensor map, then get coords using int as
            # the key.
            if key_str != '':
               try:
                  with self.print_lock:
                     print "| Got from input queue: ", key_str
                  key = int(key_str)
                  # Step the state table to update sensor states and allow us to
                  # identify which train has pinged a sensor.
                  self.STATE.step(key)
                  if key in self.sensor_map:
                     # This check MUST be done after STATE.step(key) to determine the new
                     # state of the sensor.
                     print "KEY: ", key
                     if self.STATE.is_new_occupation(key):
                        print "NEW OCCUPATION, KEY: ", key
                        msg = _build_msg(self.sensor_map, key)
                        # Based on the known initialisation constraints on train location and the
                        # tracked state each train, identify which TBU must be given this
                        # location update.
                        # Pop it on the outgoing queue for this client TBU
                        self.out_queues[self.STATE.get_train_number_from_sensor(
                           key) - 1].put(msg)
               except ValueError:
                  print "VALUE ERROR - MessagePacker.run"
                  pass

   def stop(self):
      """ So that we can kill the process in testing.
      """
      self.running = False

   def pause(self):
      """
      """
      with self.print_lock: print "| Paused Message Packing"
      self.paused = True
      self.pause_cond.acquire()

   def resume(self):
      """
      """
      with self.print_lock: print "| Resumed Message Packing"
      self.paused = False
      self.pause_cond.notify()
      self.pause_cond.release()

   def is_paused(self):
      """ Return paused.
      """
      return self.paused


#------------------------------------------------------------------------------
# 'Private' functions.


def _getNMEACoordinateFromDecimal(dd, isLongitude):
   """
   """
   num = abs(float(dd))
   d = float(math.floor(num))
   m = num - d
   # Sign should be prepended to return but we are manually adding the
   # NSEW so just abs it.
   # sign = '-' if float(dd) < 0 else ''
   if isLongitude:
      return '%03i%02.5f' % (int(d), m * 60.00)
   else:
      return '%02i%02.5f' % (int(d), m * 60.00)


def _send_initial_locations(STATE, out_queues, sensor_map):
   """
   """
   for start_sensor in STATE.start_sensors:
      msg = _build_msg(sensor_map, start_sensor)
      # Based on the known initialisation constraints on train location and the
      # tracked state each train, identify which TBU must be given this
      # location update.
      # Pop it on the outgoing queue for this client TBU
      out_queues[STATE.get_train_number_from_sensor(start_sensor) - 1].put(msg)
      out_queues[STATE.get_train_number_from_sensor(start_sensor) - 1].put(msg)
      out_queues[STATE.get_train_number_from_sensor(start_sensor) - 1].put(msg)


def _build_msg(sensor_map, key):
   """
   """
   lat_long = sensor_map[key]
   # Create the ModelRailwayPositionData message
   un = datetime.utcnow()
   utctime = str(un)[str(un).find(" ") + 1:-4].replace(":", "")
   print "| Building message from lat-long:", lat_long["NMEA_lat"], lat_long[
      "NMEA_long"]
   msg = MessageTemplates.make_ModelRailwayPositionData(
      utctime, _getNMEACoordinateFromDecimal(lat_long["NMEA_lat"], False), "N",
      _getNMEACoordinateFromDecimal(lat_long["NMEA_long"], True), "W", 7, 10)
   msg = msg + chr(10)
   return msg