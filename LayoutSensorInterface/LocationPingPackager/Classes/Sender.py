#!/usr/bin/python2
"""   A generic sender class that can be instanced for each client address
      needed by the BDCS.
"""

import threading
import socket
import Queue
import time
import sys

#------------------------------------------------------------------------------

class Sender(threading.Thread):
   def __init__(self, print_lock, protocol, lock, queue, ip, port, port_queue=None):
      threading.Thread.__init__(self)
      self.print_lock = print_lock
      self.lock = lock
      self.queue = queue
      self.ip = ip
      self.port = port
      self.port_queue = port_queue
      self.queue = queue
      self.using_tcp = (protocol == 'TCP/IP')
      if self.using_tcp:
         self.sock = socket.socket(socket.AF_INET,      # Internet
                                   socket.SOCK_STREAM)  # TCP/IP
         connected = False
         while not connected:
            try:
               self.sock.connect((ip, port))
               connected = True
            except:
               time.sleep(5)
               print "Failed to connect to %s %s" % (ip, port)
               print sys.exc_info()[0]
      else :
         self.sock = socket.socket(socket.AF_INET,      # Internet
                                   socket.SOCK_DGRAM)   # UDP
      self.running = True
      #self.sock.settimeout(5.0)

   def run(self):
      with self.print_lock: print "| Opening client socket at: %s, %s" % (self.ip, self.port)
      while self.running:
         time.sleep(0.1)
         if self.port_queue is not None:
            self.port = self.get_port_from_queue()
         # Should have a better check here what is range of ports?
         if 0 < self.port < 62000:
            try:
               msg = self.queue.get(False)
               if type(msg) == str:
                  if self.port != 0:
                     with self.print_lock: print "| Sending message to %s:%s:\n" % (self.ip, self.port), msg
                  if self.using_tcp:
                     self.sock.sendall(msg)
                  else:
                     self.sock.sendto(msg, (self.ip, self.port))
                  # if "RESET" in msg:
                  #    self.running = False
               # Rather dodgy, but we get a tuple from the queue it is because
               # we are emant to read the message from msg[0] and the IP from
               # msg[1].
               if type(msg) == tuple:
                  if type(msg[0]) != int:
                     if self.port != 0:
                        with self.print_lock:
                           print "| Sending message to %s:%s:\n" % (msg[1], self.port), msg[0]
                     self.sock.sendto(msg[0], (msg[1], self.port))
                  else:
                     with self.print_lock:
                        print "| Sending message to %s:%s:\n" % (self.ip, self.port), msg[1]
                     self.sock.sendto(msg[1], (self.ip, self.port))
            except Queue.Empty:
               pass
      # No longer running.
      with self.print_lock:
         print "| Closing client socket at: %s, %s" % (self.ip, self.port)
      try:
         if self.using_tcp:
            self.sock.sendall("CLOSE")
         else:
            self.sock.sendto("CLOSE", (self.ip, self.port))
      except:
         pass
      self.sock.close()

   def stop(self):
      self.running = False

   def get_port_from_queue(self):
      port_num = 0
      with self.lock:
         try:
            port_num = self.port_queue.get(False)
            # And put it straigt back on the queue...
            # Surely a better way for instance threads to share data as a read only
            # type, although maybe not.
            self.port_queue.put(port_num)
         except Queue.Empty:
            pass
      return port_num