import math


def getNMEACoordinateFromDecimal(dd, isLongitude):
   """
   """
   num = abs(float(dd))
   d = float(math.floor(num))
   m = num - d
   #sign = '-' if float(dd) < 0 else ''
   if isLongitude:
      return '%03i%02.5f' % (int(d), m * 60.00)
   else:
      return '%02i%02.5f' % (int(d), m * 60.00)


SENSOR_MAP = {}
SENSOR_MAP[1] = {
   "NMEA_lat" : " 51.6204797  ",
   "NMEA_long": "  -1.2930861  "
}
SENSOR_MAP[2] = {
   "NMEA_lat" : " 51.62099  ",
   "NMEA_long": "  -1.303992  "
}

#51.62099, -1.303992

print getNMEACoordinateFromDecimal(SENSOR_MAP[2]["NMEA_lat"], False)

print getNMEACoordinateFromDecimal(SENSOR_MAP[2]["NMEA_long"], True)