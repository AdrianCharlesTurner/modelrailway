#!/bin/sh
export DISPLAY=:0
./start_vnc.sh &
# Variable to change the train headcode every time it restarts
h0=1
h1="A"
h2=1
h3=1

# If reset event occurs we want to re-run the
# run_tbu.sh script.
until ./run_tbu.sh $h0$h1$h2$h3; do
   echo "+++    Resetting the TBU    +++"
   sleep 1
   ((h0++))
   if (($h0=="10"))
   then
      ((h2++))
      # $h2=$h2+1
      $h0=1
   fi
   if (($h2=="10"))
   then
      $h2=1
      #$h3=$h3+1
      ((h3++))
   fi
   if (($h3=="10"))
   then
      $h3=1
      h1="B"
   fi
done
